# 使い方
## 実行手順

### git clone

```
git clone https://gitlab.com/skitamur/aws-terraform.git
cd aws-terraform/autostartstop_function
```

### AWS認証情報を環境変数に設定する
```
export AWS_ACCESS_KEY_ID=AKIXXXXXXXXXXXXXXXX
export AWS_SECRET_ACCESS_KEY=XXXXXXXXXXXXXXXXXXXXX
```

### local.tfを書き換える
* 自分の環境に適したregionを指定する。（デフォルトはap-northeast-1）
* 下記の「設定の変更」を参照しながら適した設定に書き換える

### 実行

```
terraform init
terraform apply
```


## 設定の変更
* 変更が必要そうな箇所はlocal.tfに切り出しています。
* 以下にいくつかのユースケースを記載します。

### lambda functionのコードを修正する
* src配下に実行コードを格納しています。
* 適宜修正してapplyかければ、変更されたコードがデプロイされるようになっています。

### 自動起動・停止の時間を修正する
* local.auto[start/stop]_event_rule.schedule_expressionのcronを修正してください。
* UTC記載です。デフォルトは月〜金の9:00-21:00で動作します。

### 自動起動・停止をON/OFFする
* local.auto[start/stop]_event_rule.is_enabledをfalseにすると自動起動・停止をOFFにできます。
* 自動停止はしたいけど、起動は自分のタイミングでしたい、という場合はこちらをお使いください。

### 対象のインスタンスを指定する
* local.auto[start/stop]_lambda_function.variablesにて、INSTANCESという環境変数に配列で指定すれば実行対象を指定できます。
* INSTANCES環境変数の指定がなければ、全インスタンスを対象とします（デフォルトではコメントアウト）。