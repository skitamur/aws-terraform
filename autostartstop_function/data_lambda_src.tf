# Lambda File archive to Zip
data "archive_file" "autostart_lambda_file" {
  type        = local.autostart_lambda_file.archive_file_type
  source_dir  = local.autostart_lambda_file.lambda_source_dir
  output_path = local.autostart_lambda_file.deploy_upload_filename
}

# Lambda File archive to Zip
data "archive_file" "autostop_lambda_file" {
  type        = local.autostop_lambda_file.archive_file_type
  source_dir  = local.autostop_lambda_file.lambda_source_dir
  output_path = local.autostop_lambda_file.deploy_upload_filename
}