
resource "aws_iam_role" "lambda_role" {
  name               = local.iam_role.name
  assume_role_policy = local.iam_role.assume_role_policy
}

resource "aws_iam_policy" "lambda_policy" {
  name   = local.iam_policy.name
  policy = local.iam_policy.policy
}

resource "aws_iam_policy_attachment" "lambda-iam-attach" {
  name = "lambda-iam-attachment"
  roles = [
    aws_iam_role.lambda_role.name
  ]
  policy_arn = aws_iam_policy.lambda_policy.arn
}