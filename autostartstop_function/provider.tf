provider "aws" {
  #Set the access key to the environment variable "AWS_ACCESS_KEY_ID".
  #Set the secret key to the environment variable "AWS_SECRET_ACCESS_KEY".
  region = local.region
}
provider "archive" {}