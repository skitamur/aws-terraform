locals {
  region = "ap-northeast-1"

  iam_role = {
    name               = "AutoStartStopRoleForLambda"
    assume_role_policy = <<EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOT
  }

  iam_policy = {
    name   = "AutoStartStopPolicy"
    policy = <<EOT
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "ec2:Start*",
        "ec2:Stop*",
        "ec2:Describe*"
      ],
      "Resource": "*"
    }
  ]
}
EOT
  }

  autostart_lambda_function = {
    filename      = "autostart.py"
    function_name = "StartEC2Instances"
    runtime       = "python3.9"
    timeout       = 10
    handler       = "autostart.lambda_handler"

    variables = {
      REGION = local.region
      # If INSTANCES is not set, then all instances will be targeted.
      #INSTANCES = "[i-XXXXXXXXXXX,i-XXXXXXXXXXX]"
    }
  }

  autostop_lambda_function = {
    filename      = "autostop.py"
    function_name = "StopEC2Instances"
    runtime       = "python3.9"
    timeout       = 10
    handler       = "autostop.lambda_handler"

    variables = {
      REGION = local.region
      # If INSTANCES is not set, then all instances will be targeted.
      #INSTANCES = "[i-XXXXXXXXXXX,i-XXXXXXXXXXX]"
    }
  }

  autostart_lambda_file = {
    archive_file_type      = "zip"
    lambda_source_dir      = "./src/autostart"
    deploy_upload_filename = "./src/autostart_lambda_src.zip"
  }

  autostop_lambda_file = {
    archive_file_type      = "zip"
    lambda_source_dir      = "./src/autostop"
    deploy_upload_filename = "./src/autostop_lambda_src.zip"
  }

  autostart_event_rule = {
    name        = "StartEC2Instances"
    description = "Start EC2 Instances"

    # Run at 0:00 (UTC) every Monday through Friday
    schedule_expression = "cron(0 0 ? * MON-FRI *)"
    is_enabled          = true
  }

  autostop_event_rule = {
    name        = "StopEC2Instances"
    description = "Stop EC2 Instances"

    # Run at 12:00 (UTC) every Monday through Friday
    schedule_expression = "cron(0 12 ? * MON-FRI *)"
    is_enabled          = true
  }
}

