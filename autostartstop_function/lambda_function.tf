resource "aws_lambda_function" "autostart_lambda" {
  filename         = data.archive_file.autostart_lambda_file.output_path
  function_name    = local.autostart_lambda_function.function_name
  role             = aws_iam_role.lambda_role.arn
  handler          = local.autostart_lambda_function.handler
  source_code_hash = data.archive_file.autostart_lambda_file.output_base64sha256
  runtime          = local.autostart_lambda_function.runtime
  timeout          = local.autostart_lambda_function.timeout

  environment {
    variables = local.autostart_lambda_function.variables
  }
}

resource "aws_lambda_function" "autostop_lambda" {
  filename         = data.archive_file.autostop_lambda_file.output_path
  function_name    = local.autostop_lambda_function.function_name
  role             = aws_iam_role.lambda_role.arn
  handler          = local.autostop_lambda_function.handler
  source_code_hash = data.archive_file.autostop_lambda_file.output_base64sha256
  runtime          = local.autostop_lambda_function.runtime
  timeout          = local.autostop_lambda_function.timeout

  environment {
    variables = local.autostop_lambda_function.variables
  }
}