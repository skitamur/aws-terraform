# Auto Start Rule
resource "aws_cloudwatch_event_rule" "autostart_rule" {
  name                = local.autostart_event_rule.name
  description         = local.autostart_event_rule.description
  schedule_expression = local.autostart_event_rule.schedule_expression
  is_enabled          = local.autostart_event_rule.is_enabled
}

# Auto Start Target
resource "aws_cloudwatch_event_target" "autostart_target" {
  arn  = aws_lambda_function.autostart_lambda.arn
  rule = aws_cloudwatch_event_rule.autostart_rule.id
}

# Auto Stop Rule
resource "aws_cloudwatch_event_rule" "autostop_rule" {
  name                = local.autostop_event_rule.name
  description         = local.autostop_event_rule.description
  schedule_expression = local.autostop_event_rule.schedule_expression
  is_enabled          = local.autostop_event_rule.is_enabled
}

# Auto Stop Target
resource "aws_cloudwatch_event_target" "autostop_target" {
  arn  = aws_lambda_function.autostop_lambda.arn
  rule = aws_cloudwatch_event_rule.autostop_rule.id
}