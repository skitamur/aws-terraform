import boto3
import os

region = os.environ.get('REGION','ap-northeast-1')
ec2 = boto3.client('ec2', region_name=region)

ec2_data = ec2.describe_instances(
    Filters=[{'Name': 'instance-state-name', 'Values': ['stopped']}]
)
ec2_instance_id = list()
for ec2_reservation in ec2_data['Reservations']:
    for ec2_instance in ec2_reservation['Instances']:
        ec2_instance_id.append(ec2_instance['InstanceId'])

instances = os.environ.get('INSTNACES',ec2_instance_id)

def lambda_handler(event, context):
    ec2.start_instances(InstanceIds=instances)
    print('started your instances: ' + str(instances))


