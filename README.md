# autostartstop_function
## 概要
* AWSの利用料削減のために、任意の時間にてEC2インスタンスの自動起動・自動停止を実現するツールです。
* EventBridge(旧CloudWatch)、Lambdaを使って実現します。
* 検証環境での利用を想定しているため、デフォルトでは特定のRegionにおける全インスタンスを対象に自動起動・停止を行います。
* インスタンスを指定するパラメータも準備していますので、必要に応じてご利用下さい。
* タグとの連携など、より高度な制御を行う際は適宜pythonスクリプトを修正ください。

## 前提条件
* 対象のAWSアカウントのIAMユーザー(AdministratorAccess)
* terrafrom v1.1.2

## 使い方
* autostartstop_function内のREADMEを参照